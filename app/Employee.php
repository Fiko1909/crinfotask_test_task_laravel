<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function get_person(){
        return $this->belongsTo('App\Person', 'person_id');
    }

    public function get_position(){
        return $this->belongsTo('App\Position', 'position_id');
    }
}
