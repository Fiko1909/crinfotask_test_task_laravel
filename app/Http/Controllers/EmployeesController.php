<?php

namespace App\Http\Controllers;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeesController extends Controller
{
    public function get(){
        $employees = Employee::all();
        return response()->json($employees);
    }

    public function post(Request $request){
        $validator = Validator::make($request->all(), [
            'person_id'=>'required|max:255',
            'position_id'=>'required|max:255',
            'start_date'=>'required|max:255',
            'dismissal_date'=>'required|max:255',
            'salary'=>'required|max:255',
            'created_by'=>'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }


        $employee = new Employee();
        $employee->person_id = $request->person_id;
        $employee->position_id = $request->position_id;
        $employee->start_date = $request->start_date;
        $employee->dismissal_date = $request->dismissal_date;
        $employee->salary = $request->salary;
        $employee->created_by = $request->created_by;
        $employee->save();
        return response()->json($employee);
    }

    public function delete($id){
        $employee = Employee::findOrFail($id);
        $employee->delete();
        return response()->json('Employee Deleted');
    }

    public function patch(Request $request,$id){
        $employee = Employee::findOrFail($id);
        $employee->person_id = $request->person_id;
        $employee->position_id = $request->position_id;
        $employee->start_date = $request->start_date;
        $employee->dismissal_date = $request->dismissal_date;
        $employee->salary = $request->salary;
        $employee->created_by = $request->created_by;
        $employee->save();
        return response()->json($employee);
    }

    public function get_full_data(){
        $employees = Employee::with('get_person','get_position')->get();
        return response()->json($employees);
    }
}
