<?php

namespace App\Http\Controllers;
Use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PersonsController extends Controller
{
    public function get(){
        $persons = Person::all();
        return response()->json($persons);
    }

    public function post(Request $request){
        $validator = Validator::make($request->all(), [
            'full_name'=>'required|max:255',
            'birthday'=>'required|max:255',
            'birth_place'=>'required|max:255',
            'doc_number'=>'required|max:255',
            'doc_pin_code'=>'required|max:255',
            'created_by'=>'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }


        $person = new Person();
        $person->full_name = $request->full_name;
        $person->birthday = $request->birthday;
        $person->birth_place = $request->birth_place;
        $person->doc_number = $request->doc_number;
        $person->doc_pin_code = $request->doc_pin_code;
        $person->created_by = $request->created_by;
        $person->save();
        return response()->json($person);
    }

    public function delete($id){
        $person = Person::findOrFail($id);
        $person->delete();
        return response()->json('Person Deleted');
    }

    public function patch(Request $request,$id){
        $person = Person::findOrFail($id);
        $person->full_name = $request->full_name;
        $person->birthday = $request->birthday;
        $person->birth_place = $request->birth_place;
        $person->doc_number = $request->doc_number;
        $person->doc_pin_code = $request->doc_pin_code;
        $person->created_by = $request->created_by;
        $person->save();
        return response()->json($person);
    }
}
