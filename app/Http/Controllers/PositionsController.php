<?php

namespace App\Http\Controllers;
use App\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PositionsController extends Controller
{
    public function get(){
        $positions = Position::all();
        return response()->json($positions);
    }

    public function post(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }


        $position = new Position();
        $position->name = $request->name;
        $position->description = $request->description;
        $position->save();
        return response()->json($position);
    }

    public function delete($id){
        $position = Position::findOrFail($id);
        $position->delete();
        return response()->json('Position Deleted');
    }

    public function patch(Request $request,$id){
        $position = Position::findOrFail($id);
        $position->name = $request->name;
        $position->description = $request->description;
        $position->save();
        return response()->json($position);
    }
}
