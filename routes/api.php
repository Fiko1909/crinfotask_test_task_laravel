<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {
    Route::get('persons', "PersonsController@get");
    Route::post('persons', "PersonsController@post");
    Route::delete('persons/{id}', "PersonsController@delete");
    Route::patch('persons/{id}',"PersonsController@patch");

    Route::get('positions', "PositionsController@get");
    Route::post('positions', "PositionsController@post");
    Route::delete('positions/{id}', "PositionsController@delete");
    Route::patch('positions/{id}',"PositionsController@patch");

    Route::get('employees', "EmployeesController@get");
    Route::post('employees', "EmployeesController@post");
    Route::delete('employees/{id}', "EmployeesController@delete");
    Route::patch('employees/{id}',"EmployeesController@patch");

    Route::get('employee_full_data', "EmployeesController@get_full_data");

});
